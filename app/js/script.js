/* Author: John Gibby @thatgibbyguy */

// ==========================
/* Store current location */
// ==========================

var pathName = location.pathname;

// ==========================
/* Remove Phone Link on Desktop */
// ==========================

remPhoneLink = function(){
	docWidth = $(document).width();
	if (docWidth >= 1024){
		$('[data-query="phone"]').click(function(){
			return false;
		});
	}
	else{
		$('[data-query="phone"]').click(function(){
			return true;
		});
	}
};
remPhoneLink();

// ==========================
/* Adding and removing classes */
// ==========================

var addClassTo = function(query){
	$('[data-query="' + query + '"]').addClass('on');
};
var removeClassFrom = function(query){
	$('[data-query="' + query + '"]').removeClass('on');
};
var addClassByUrl = function(path,query){
	if(pathName.indexOf(path) >= 0){
		$('[data-query="' + query + '"]').addClass('on');
	}
};

// ==========================
/* Mobile Menu Trigger */
// ==========================

var nav = '';
var toggler = document.getElementById('mobile-toggle');

$(toggler).click(function(){
	$(nav).toggleClass('mobile-hidden').toggleClass('show');
});

// ==============================================
/* JVFloat - Placeholder Effect*/
// ==============================================

//apply effect
if(!$('html').hasClass('lt-ie9')){
	$('.float-pattern').jvFloat();
}

// ==============================================
/* Disable buttons for styleguide*/
// ==============================================

[].slice.call(document.querySelectorAll('#buttons .btn,.logo')).forEach(function(el){
	el.addEventListener('click',function(e){
		e.preventDefault();
	}, false);
});

// ==============================================
/* Material Design Button*/
// ==============================================

//appends the overlay to each button
$(".btn-rip").each( function(){
$(this).append("<div class='ripple'></div>");
});


$(".btn-rip").click(function(e){
  var $clicked = $(this);
  
  //gets the clicked coordinates
  var offset = $clicked.offset();
var relativeX = (e.pageX - offset.left);
var relativeY = (e.pageY - offset.top);
  var width = $clicked.width();
  
  
  var $ripple = $clicked.find('.ripple');
  
  //puts the ripple in the clicked coordinates without animation
  $ripple.addClass("notransition");
  $ripple.css({"top" : relativeY, "left": relativeX});
  $ripple[0].offsetHeight;
  $ripple.removeClass("notransition");
  
  //animates the button and the ripple
  $clicked.addClass("hovered");
  $ripple.css({ "width": width * 2, "height": width*2, "margin-left": -width, "margin-top": -width });
  
  setTimeout(function(){
      //resets the overlay and button
      $ripple.addClass("notransition");
      $ripple.attr("style", "");
      $ripple[0].offsetHeight;
      $clicked.removeClass("hovered");
    $ripple.removeClass("notransition");
  }, 300 );
});


